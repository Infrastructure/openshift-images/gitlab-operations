FROM registry.access.redhat.com/ubi8/python-38

COPY requirements.txt /opt/app-root/src
COPY ipa-ca.crt /etc/ipa/ca.crt

USER 0

RUN pip install -r /opt/app-root/src/requirements.txt && \
    install -d /home/admin && \
    git clone https://gitlab.gnome.org/Infrastructure/sysadmin-bin.git /home/admin/bin && \
    echo 'TLS_CACERT /etc/ipa/ca.crt' > /etc/openldap/ldap.conf
